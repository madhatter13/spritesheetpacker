# Sprite Sheet Packer #

This is an unofficial fork of Sprite Sheet Packer v2.3 (25 Mar 2010)

Official homepage: https://spritesheetpacker.codeplex.com/

Note that this is my own personal fork where I've just made some changes I required. Please feel free to make your own fork if you want to make further changes.

Changes from official version:

* Applied patches by TOgburn (11334 & 11743) and Dlaha (14000) (Source: https://spritesheetpacker.codeplex.com/SourceControl/list/patches)
* Fixed some bugs related to auto-crop
* Removed separate "crop" file which was created when auto-crop was enabled (map exporter can now decide how this information will be written)
* Added MonoGame sprite sheet map exporter
* Implemented "Add Folder" button in GUI
* Added support for animated GIFs
* Improved efficiency of image packing
* Added support for image rotation