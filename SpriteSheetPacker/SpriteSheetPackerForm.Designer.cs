﻿#region MIT License

/*
 * Copyright (c) 2009 Nick Gravelyn (nick@gravelyn.com), Markus Ewald (cygon@nuclex.org)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * 
 */

#endregion

namespace SpriteSheetPacker
{
	partial class SpriteSheetPackerForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.removeImageBtn = new System.Windows.Forms.Button();
            this.addImageBtn = new System.Windows.Forms.Button();
            this.buildBtn = new System.Windows.Forms.Button();
            this.imageOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.clearBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.paddingTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maxWidthTxtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.maxHeightTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.imageFileTxtBox = new System.Windows.Forms.TextBox();
            this.browseImageBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.mapFileTxtBox = new System.Windows.Forms.TextBox();
            this.browseMapBtn = new System.Windows.Forms.Button();
            this.imageSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mapSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.powOf2CheckBox = new System.Windows.Forms.CheckBox();
            this.squareCheckBox = new System.Windows.Forms.CheckBox();
            this.generateMapCheckBox = new System.Windows.Forms.CheckBox();
            this.autoCropCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxCropSameSize = new System.Windows.Forms.CheckBox();
            this.txtCommandLine = new System.Windows.Forms.TextBox();
            this.lblCommandLine = new System.Windows.Forms.Label();
            this.imageFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.addImageFolderButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.allowRotationCheckBox = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.AllowDrop = true;
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.IntegralHeight = false;
            this.listBox1.Location = new System.Drawing.Point(11, 11);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox1.Size = new System.Drawing.Size(528, 150);
            this.listBox1.TabIndex = 0;
            this.listBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox1_DragDrop);
            this.listBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBox1_DragEnter);
            // 
            // removeImageBtn
            // 
            this.removeImageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeImageBtn.Location = new System.Drawing.Point(335, 165);
            this.removeImageBtn.Margin = new System.Windows.Forms.Padding(2);
            this.removeImageBtn.Name = "removeImageBtn";
            this.removeImageBtn.Size = new System.Drawing.Size(100, 28);
            this.removeImageBtn.TabIndex = 3;
            this.removeImageBtn.Text = "Remove Selected";
            this.removeImageBtn.UseVisualStyleBackColor = true;
            this.removeImageBtn.Click += new System.EventHandler(this.removeImageBtn_Click);
            // 
            // addImageBtn
            // 
            this.addImageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addImageBtn.Location = new System.Drawing.Point(231, 165);
            this.addImageBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addImageBtn.Name = "addImageBtn";
            this.addImageBtn.Size = new System.Drawing.Size(100, 28);
            this.addImageBtn.TabIndex = 2;
            this.addImageBtn.Text = "Add Images";
            this.addImageBtn.UseVisualStyleBackColor = true;
            this.addImageBtn.Click += new System.EventHandler(this.addImageBtn_Click);
            // 
            // buildBtn
            // 
            this.buildBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buildBtn.Location = new System.Drawing.Point(358, 116);
            this.buildBtn.Margin = new System.Windows.Forms.Padding(2);
            this.buildBtn.Name = "buildBtn";
            this.buildBtn.Size = new System.Drawing.Size(172, 41);
            this.buildBtn.TabIndex = 13;
            this.buildBtn.Text = "Build Sprite Sheet";
            this.buildBtn.UseVisualStyleBackColor = true;
            this.buildBtn.Click += new System.EventHandler(this.buildBtn_Click);
            // 
            // imageOpenFileDialog
            // 
            this.imageOpenFileDialog.AddExtension = false;
            this.imageOpenFileDialog.FileName = "openFileDialog1";
            this.imageOpenFileDialog.Filter = "Image Files (png, jpg, bmp)|*.png;*.jpg;*.bmp";
            this.imageOpenFileDialog.Multiselect = true;
            // 
            // clearBtn
            // 
            this.clearBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clearBtn.Location = new System.Drawing.Point(439, 165);
            this.clearBtn.Margin = new System.Windows.Forms.Padding(2);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(100, 28);
            this.clearBtn.TabIndex = 4;
            this.clearBtn.Text = "Remove All";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Image padding:";
            // 
            // paddingTxtBox
            // 
            this.paddingTxtBox.Location = new System.Drawing.Point(85, 91);
            this.paddingTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.paddingTxtBox.Name = "paddingTxtBox";
            this.paddingTxtBox.Size = new System.Drawing.Size(76, 20);
            this.paddingTxtBox.TabIndex = 4;
            this.paddingTxtBox.Text = "0";
            this.paddingTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Maximum sheet size:";
            // 
            // maxWidthTxtBox
            // 
            this.maxWidthTxtBox.Location = new System.Drawing.Point(27, 21);
            this.maxWidthTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.maxWidthTxtBox.Name = "maxWidthTxtBox";
            this.maxWidthTxtBox.Size = new System.Drawing.Size(76, 20);
            this.maxWidthTxtBox.TabIndex = 0;
            this.maxWidthTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "x";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // maxHeightTxtBox
            // 
            this.maxHeightTxtBox.Location = new System.Drawing.Point(123, 21);
            this.maxHeightTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.maxHeightTxtBox.Name = "maxHeightTxtBox";
            this.maxHeightTxtBox.Size = new System.Drawing.Size(76, 20);
            this.maxHeightTxtBox.TabIndex = 1;
            this.maxHeightTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(222, 4);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 14);
            this.label5.TabIndex = 2;
            this.label5.Text = "Image file:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // imageFileTxtBox
            // 
            this.imageFileTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imageFileTxtBox.Location = new System.Drawing.Point(284, 2);
            this.imageFileTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.imageFileTxtBox.Name = "imageFileTxtBox";
            this.imageFileTxtBox.ReadOnly = true;
            this.imageFileTxtBox.Size = new System.Drawing.Size(216, 20);
            this.imageFileTxtBox.TabIndex = 8;
            // 
            // browseImageBtn
            // 
            this.browseImageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseImageBtn.Location = new System.Drawing.Point(504, 2);
            this.browseImageBtn.Margin = new System.Windows.Forms.Padding(2);
            this.browseImageBtn.Name = "browseImageBtn";
            this.browseImageBtn.Size = new System.Drawing.Size(26, 20);
            this.browseImageBtn.TabIndex = 9;
            this.browseImageBtn.Text = "...";
            this.browseImageBtn.UseVisualStyleBackColor = true;
            this.browseImageBtn.Click += new System.EventHandler(this.browseImageBtn_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(222, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 14);
            this.label6.TabIndex = 2;
            this.label6.Text = "Map file:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mapFileTxtBox
            // 
            this.mapFileTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mapFileTxtBox.Location = new System.Drawing.Point(284, 26);
            this.mapFileTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.mapFileTxtBox.Name = "mapFileTxtBox";
            this.mapFileTxtBox.ReadOnly = true;
            this.mapFileTxtBox.Size = new System.Drawing.Size(216, 20);
            this.mapFileTxtBox.TabIndex = 10;
            // 
            // browseMapBtn
            // 
            this.browseMapBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseMapBtn.Location = new System.Drawing.Point(504, 26);
            this.browseMapBtn.Margin = new System.Windows.Forms.Padding(2);
            this.browseMapBtn.Name = "browseMapBtn";
            this.browseMapBtn.Size = new System.Drawing.Size(26, 20);
            this.browseMapBtn.TabIndex = 11;
            this.browseMapBtn.Text = "...";
            this.browseMapBtn.UseVisualStyleBackColor = true;
            this.browseMapBtn.Click += new System.EventHandler(this.browseMapBtn_Click);
            // 
            // imageSaveFileDialog
            // 
            this.imageSaveFileDialog.DefaultExt = "png";
            this.imageSaveFileDialog.Filter = "PNG Files|*.png";
            // 
            // mapSaveFileDialog
            // 
            this.mapSaveFileDialog.DefaultExt = "txt";
            this.mapSaveFileDialog.Filter = "TXT Files|*.txt";
            // 
            // powOf2CheckBox
            // 
            this.powOf2CheckBox.AutoSize = true;
            this.powOf2CheckBox.Location = new System.Drawing.Point(27, 45);
            this.powOf2CheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.powOf2CheckBox.Name = "powOf2CheckBox";
            this.powOf2CheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.powOf2CheckBox.Size = new System.Drawing.Size(117, 17);
            this.powOf2CheckBox.TabIndex = 2;
            this.powOf2CheckBox.Text = "Must be power of 2";
            this.powOf2CheckBox.UseVisualStyleBackColor = true;
            // 
            // squareCheckBox
            // 
            this.squareCheckBox.AutoSize = true;
            this.squareCheckBox.Location = new System.Drawing.Point(27, 66);
            this.squareCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.squareCheckBox.Name = "squareCheckBox";
            this.squareCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.squareCheckBox.Size = new System.Drawing.Size(99, 17);
            this.squareCheckBox.TabIndex = 3;
            this.squareCheckBox.Text = "Must be square";
            this.squareCheckBox.UseVisualStyleBackColor = true;
            // 
            // generateMapCheckBox
            // 
            this.generateMapCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateMapCheckBox.AutoSize = true;
            this.generateMapCheckBox.Checked = true;
            this.generateMapCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.generateMapCheckBox.Location = new System.Drawing.Point(284, 50);
            this.generateMapCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.generateMapCheckBox.Name = "generateMapCheckBox";
            this.generateMapCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.generateMapCheckBox.Size = new System.Drawing.Size(109, 17);
            this.generateMapCheckBox.TabIndex = 12;
            this.generateMapCheckBox.Text = "Generate map file";
            this.generateMapCheckBox.UseVisualStyleBackColor = true;
            // 
            // autoCropCheckBox
            // 
            this.autoCropCheckBox.AutoSize = true;
            this.autoCropCheckBox.Location = new System.Drawing.Point(2, 118);
            this.autoCropCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.autoCropCheckBox.Name = "autoCropCheckBox";
            this.autoCropCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.autoCropCheckBox.Size = new System.Drawing.Size(108, 17);
            this.autoCropCheckBox.TabIndex = 5;
            this.autoCropCheckBox.Text = "Auto crop images";
            this.autoCropCheckBox.UseVisualStyleBackColor = true;
            this.autoCropCheckBox.CheckedChanged += new System.EventHandler(this.autoCropCheckBox_CheckedChanged);
            // 
            // checkBoxCropSameSize
            // 
            this.checkBoxCropSameSize.AutoSize = true;
            this.checkBoxCropSameSize.Enabled = false;
            this.checkBoxCropSameSize.Location = new System.Drawing.Point(114, 118);
            this.checkBoxCropSameSize.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxCropSameSize.Name = "checkBoxCropSameSize";
            this.checkBoxCropSameSize.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBoxCropSameSize.Size = new System.Drawing.Size(122, 17);
            this.checkBoxCropSameSize.TabIndex = 6;
            this.checkBoxCropSameSize.Text = "Crop all to same size";
            this.checkBoxCropSameSize.UseVisualStyleBackColor = true;
            // 
            // txtCommandLine
            // 
            this.txtCommandLine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommandLine.Location = new System.Drawing.Point(85, 167);
            this.txtCommandLine.Name = "txtCommandLine";
            this.txtCommandLine.ReadOnly = true;
            this.txtCommandLine.Size = new System.Drawing.Size(445, 20);
            this.txtCommandLine.TabIndex = 14;
            // 
            // lblCommandLine
            // 
            this.lblCommandLine.AutoSize = true;
            this.lblCommandLine.Location = new System.Drawing.Point(-1, 170);
            this.lblCommandLine.Name = "lblCommandLine";
            this.lblCommandLine.Size = new System.Drawing.Size(80, 13);
            this.lblCommandLine.TabIndex = 16;
            this.lblCommandLine.Text = "Command Line:";
            // 
            // imageFolderBrowserDialog
            // 
            this.imageFolderBrowserDialog.ShowNewFolderButton = false;
            // 
            // addImageFolderButton
            // 
            this.addImageFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addImageFolderButton.Location = new System.Drawing.Point(127, 165);
            this.addImageFolderButton.Margin = new System.Windows.Forms.Padding(2);
            this.addImageFolderButton.Name = "addImageFolderButton";
            this.addImageFolderButton.Size = new System.Drawing.Size(100, 28);
            this.addImageFolderButton.TabIndex = 1;
            this.addImageFolderButton.Text = "Add Folder";
            this.addImageFolderButton.UseVisualStyleBackColor = true;
            this.addImageFolderButton.Click += new System.EventHandler(this.addImageFolderButton_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.allowRotationCheckBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buildBtn);
            this.panel1.Controls.Add(this.lblCommandLine);
            this.panel1.Controls.Add(this.paddingTxtBox);
            this.panel1.Controls.Add(this.txtCommandLine);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.checkBoxCropSameSize);
            this.panel1.Controls.Add(this.imageFileTxtBox);
            this.panel1.Controls.Add(this.autoCropCheckBox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.generateMapCheckBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.squareCheckBox);
            this.panel1.Controls.Add(this.mapFileTxtBox);
            this.panel1.Controls.Add(this.powOf2CheckBox);
            this.panel1.Controls.Add(this.maxWidthTxtBox);
            this.panel1.Controls.Add(this.browseMapBtn);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.browseImageBtn);
            this.panel1.Controls.Add(this.maxHeightTxtBox);
            this.panel1.Location = new System.Drawing.Point(9, 202);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 187);
            this.panel1.TabIndex = 5;
            // 
            // allowRotationCheckBox
            // 
            this.allowRotationCheckBox.AutoSize = true;
            this.allowRotationCheckBox.Location = new System.Drawing.Point(2, 140);
            this.allowRotationCheckBox.Name = "allowRotationCheckBox";
            this.allowRotationCheckBox.Size = new System.Drawing.Size(120, 17);
            this.allowRotationCheckBox.TabIndex = 7;
            this.allowRotationCheckBox.Text = "Allow image rotation";
            this.allowRotationCheckBox.UseVisualStyleBackColor = true;
            // 
            // SpriteSheetPackerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 398);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.addImageFolderButton);
            this.Controls.Add(this.addImageBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.removeImageBtn);
            this.Controls.Add(this.listBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(555, 402);
            this.Name = "SpriteSheetPackerForm";
            this.Text = "Sprite Sheet Packer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button removeImageBtn;
		private System.Windows.Forms.Button addImageBtn;
		private System.Windows.Forms.Button buildBtn;
		private System.Windows.Forms.OpenFileDialog imageOpenFileDialog;
		private System.Windows.Forms.Button clearBtn;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox paddingTxtBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox maxWidthTxtBox;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox maxHeightTxtBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox imageFileTxtBox;
		private System.Windows.Forms.Button browseImageBtn;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox mapFileTxtBox;
		private System.Windows.Forms.Button browseMapBtn;
		private System.Windows.Forms.SaveFileDialog imageSaveFileDialog;
		private System.Windows.Forms.SaveFileDialog mapSaveFileDialog;
		private System.Windows.Forms.CheckBox powOf2CheckBox;
		private System.Windows.Forms.CheckBox squareCheckBox;
		private System.Windows.Forms.CheckBox generateMapCheckBox;
		private System.Windows.Forms.CheckBox autoCropCheckBox;
		private System.Windows.Forms.CheckBox checkBoxCropSameSize;
		private System.Windows.Forms.TextBox txtCommandLine;
		private System.Windows.Forms.Label lblCommandLine;
        private System.Windows.Forms.FolderBrowserDialog imageFolderBrowserDialog;
        private System.Windows.Forms.Button addImageFolderButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox allowRotationCheckBox;
	}
}

