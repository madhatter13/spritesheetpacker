﻿namespace sspack
{
    public struct ColorKey
    {
        public readonly bool HasR;
        public readonly bool HasG;
        public readonly bool HasB;

        public ColorKey(bool hasR, bool hasG, bool hasB)
        {
            HasR = hasR;
            HasG = hasG;
            HasB = hasB;
        }
    }
}