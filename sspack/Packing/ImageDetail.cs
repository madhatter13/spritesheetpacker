﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace sspack
{
    public class ImageDetail : IDisposable, IComparable<ImageDetail>
    {
        public string FullPath { get; set; }
        public string Name { get; set; }

        private Bitmap _bitmap;
        public Bitmap Bitmap
        {
            get { return IsDupe ? _original.Bitmap : _bitmap; }
        }
        
        public Size OriginalSize { get; private set; }
        public Rectangle CropRectangle { get; private set; }
        
        private Point _location;
        public Point Location
        {
            get { return IsDupe ? _original.Location : _location; }
            set { _location = value; }
        }

        private bool _isRotated;
        public bool IsRotated
        {
            get { return IsDupe ? _original.IsRotated : _isRotated; }
            set { _isRotated = value; }
        }

        private ImageDetail _original;
        public bool IsDupe { get { return _original != null; } }

        public Rectangle OutputRectange { get { return new Rectangle(Location, Bitmap.Size); } }

        public int LongestSide => Math.Max(Bitmap.Width, Bitmap.Height);
        public int ShortestSide => Math.Min(Bitmap.Width, Bitmap.Height);

        private ImageDetail()
        {
        }

        public ImageDetail(string fileName, int scale = 1)
        {
            FullPath = Path.GetFullPath(fileName);
            Name = Path.GetFileNameWithoutExtension(fileName);
            InitialiseBitmap((Bitmap)Image.FromFile(fileName), scale);
        }

        public ImageDetail(ImageDetail imageDetail, int frameIndex)
        {
            FullPath = imageDetail.FullPath;
            Name = imageDetail.Name + "_#" + frameIndex.ToString("00");

            imageDetail.Bitmap.SelectActiveFrame(FrameDimension.Time, frameIndex);
            InitialiseBitmap(imageDetail.Bitmap.Clone(imageDetail.CropRectangle, imageDetail.Bitmap.PixelFormat));
        }

        public static ImageDetail CreateKeyedLayer(ImageDetail imageDetail, int layerNo, ColorKey colorKey)
        {
            var layerImageDetail = new ImageDetail();
            layerImageDetail.FullPath = imageDetail.FullPath;
            layerImageDetail.Name = imageDetail.Name + layerNo;
            layerImageDetail.InitialiseBitmap(imageDetail.Bitmap.Clone(imageDetail.CropRectangle, imageDetail.Bitmap.PixelFormat));

            bool isLayerPresent = false;
            for (int x = 0; x < layerImageDetail.Bitmap.Width; x++)
            {
                for (int y = 0; y < layerImageDetail.Bitmap.Height; y++)
                {
                    var pixel = layerImageDetail.Bitmap.GetPixel(x, y);
                    if (pixel.A == 0)
                    {
                        layerImageDetail.Bitmap.SetPixel(x, y, Color.Transparent);
                        continue;
                    }
                    var hasR = (pixel.R > 0);
                    var hasG = (pixel.G > 0);
                    var hasB = (pixel.B > 0);
                    var isLayerPixel = (hasR == colorKey.HasR) && (hasG == colorKey.HasG) && (hasB == colorKey.HasB);
                    if (!isLayerPixel && (layerNo == 0) && !(hasR || hasG || hasB))
                        isLayerPixel = true;
                    if (isLayerPixel)
                    {
                        var value = hasR ? pixel.R : hasG ? pixel.G : pixel.B;
                        layerImageDetail.Bitmap.SetPixel(x, y, Color.FromArgb(pixel.A, value, value, value));
                        isLayerPresent = true;
                    }
                    else
                    {
                        if ((layerNo == 0) && (pixel.A == 255))
                            layerImageDetail.Bitmap.SetPixel(x, y, Color.Black);
                        else
                            layerImageDetail.Bitmap.SetPixel(x, y, Color.Transparent);
                    }
                }
            }

            if (isLayerPresent)
                return layerImageDetail;

            layerImageDetail.Dispose();
            return null;
        }

        private void InitialiseBitmap(Bitmap bitmap, int scale = 1)
        {
            if (scale <= 1)
            {
                _bitmap = bitmap;
            }
            else
            {
                _bitmap = ResizeImage(bitmap, bitmap.Width * scale, bitmap.Height * scale);
                bitmap.Dispose();
            }
            OriginalSize = Bitmap.Size;
            CropRectangle = new Rectangle { Size = Bitmap.Size };
            Location = Point.Empty;
        }

        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public void CropImage(Rectangle cropRectangle)
        {
            if (IsDupe)
                throw new Exception("This image is a duplicate!");
            if (_bitmap.Size == cropRectangle.Size)
                return;
            CropRectangle = cropRectangle;
            var croppedBitmap = _bitmap.Clone(cropRectangle, Bitmap.PixelFormat);
            _bitmap.Dispose();
            _bitmap = croppedBitmap;
        }

        public void RotateImage()
        {
            Bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }

        public void MarkAsDupe(ImageDetail original)
        {
            _original = original;
            _bitmap?.Dispose();
            _bitmap = null;
        }

        public bool IsBitmapEqual(ImageDetail other)
        {
            if (Bitmap.Size != other.Bitmap.Size)
                return false;

            for (int y = 0; y < Bitmap.Height; y++)
                for (int x = 0; x < Bitmap.Width; x++)
                    if (Bitmap.GetPixel(x, y) != other.Bitmap.GetPixel(x, y))
                        return false;

            return true;
        }

        public int CompareTo(ImageDetail other)
        {
            if (LongestSide > other.LongestSide)
                return -1;
            if (LongestSide < other.LongestSide)
                return 1;
            if (ShortestSide > other.ShortestSide)
                return -1;
            if (ShortestSide < other.ShortestSide)
                return 1;
            return 0;
        }

        public void Dispose()
        {
            _bitmap?.Dispose();
        }
    }
}
